//
//  Defaults.swift
//  Gallery
//
//  Created by Student on 5/3/22.
//

import Foundation

class Defaults {
    let user = UserDefaults.standard
    
    func setData(login: String, password: String){
        user.set(login, forKey: "login")
        user.set(password, forKey: "password")
    }
    
    func removeData(login: String, password: String){
        user.removeObject(forKey: login)
        user.removeObject(forKey: password)
    }
    
    func getData() -> Array<String>{
        let login = user.string(forKey: "login")
        let password = user.string(forKey: "password")
        
        if login != nil && password != nil {
            return [login!, password!]
        }
        else {
            return []
        }
    }
}
