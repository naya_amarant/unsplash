//
//  FavoritesViewController.swift
//  Gallery
//
//  Created by Student on 3/24/22.
//

import UIKit
import Firebase
import FirebaseFirestore

class FavoritesViewController: UIViewController {

    @IBOutlet weak var imagesCollectionView: UICollectionView!
    
    var results: [String] = []
    
    override func viewDidLoad() {
    
        super.viewDidLoad()
        get_user_data()
        imagesCollectionView.delegate = self
        imagesCollectionView.dataSource = self
        
        imagesCollectionView.register(ImageCollectionViewCell.nib(), forCellWithReuseIdentifier: ImageCollectionViewCell.identifier)
        
        let layout = UICollectionViewFlowLayout()
        imagesCollectionView.collectionViewLayout = layout
    }
    
    // get images names to load them from folder
    func getImagesNames() -> Array<String>{
        let fm = FileManager.default
        let path = "Users/student/Downloads/saved_imgs"

        do {
            let items = try fm.contentsOfDirectory(atPath: path)

            for item in items {
                print("Found \(item)")
            }
            
            return items
            
        } catch {
            return []
        }
    }
    
    
    // loads favorite images
    func get_user_data()  {
        // if has connection, pass images urls from firestore
        if MainViewController.is_connected {
            let user_id = Auth.auth().currentUser!.uid
            let collection_name = Firestore.firestore().collection("img")
            let document_name = collection_name.document(String(user_id))
                
            document_name.getDocument { [weak self] (document, error) in
                if let document = document, document.exists {
                    let data = document.data()
                    if let data = data {
                        self?.results = data["image_url"] as? [String] ?? [""]
                        self?.imagesCollectionView.reloadData()
                    }
     
                } else {
                    print("Document does not exist")
                    return
                }
            }
        }
        // if hasn't connection, pass images titles from local
        else {
            // get all images titles from directory
            let imgs_names = self.getImagesNames()
            
            self.results = imgs_names
            
            self.imagesCollectionView.reloadData()
        }
    }
}


extension FavoritesViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let imgViewPage: ImageViewController = self.storyboard?.instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
        
        // allow like to know is it from galllery or from favorites
        imgViewPage.allow_like = false
        imgViewPage.selectedImage = results[indexPath.row]
                
        self.navigationController?.pushViewController(imgViewPage, animated: true)
    }
}

extension FavoritesViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return results.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let imageURLString = results[indexPath.row]
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.identifier, for: indexPath) as? ImageCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        // if is_connected, load images from net
        if MainViewController.is_connected {
            cell.configure(with: imageURLString)
        }
        else {
            cell.imageView.image = ImageViewController.loadImageFromDocumentDirectory(fileName: imageURLString)
        }
        
        return cell
        
    }
}

extension FavoritesViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 120)
    }
    
    // update favorite images after image has been deleted
    override func viewDidAppear(_ animated: Bool) {
        get_user_data()
    }

}


