//
//  ForgotPasswordController.swift
//  Gallery
//
//  Created by Student on 5/1/22.
//

import UIKit
import Firebase

class ForgotPasswordViewController: UIViewController {
    
    
    @IBOutlet weak var emailTextField: UITextField!
    
    
    // reset password with email link
    @IBAction func sendAction(_ sender: Any) {
        
        guard let email = emailTextField.text, !email.isEmpty
        else {
            let alert = UIAlertController(title: "Empty Email", message: "Please fill in email.", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
            return
            
        }
                    
        let auth = Auth.auth()
        
        auth.sendPasswordReset(withEmail: email) { (error) in
            if let error = error {
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
                return
            }
            else {
                let alert = UIAlertController(title: "Password reset link", message: "Password reset link was sent on your email", preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
                return
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
