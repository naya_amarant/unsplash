//
//  GalleryViewController.swift
//  Gallery
//
//  Created by Student on 3/24/22.
//



import UIKit
import Firebase

struct UnsplashResponse: Codable {
    let results: [Result]
}

struct Result: Codable {
    let id: String
    let urls: URLS
    
    let likes: Int? //+
    
    let location: Location?
    let downloads: Int?
    
}

struct URLS: Codable {
    let regular: String
}

struct Location: Codable {
    let city: String?
    let country: String?
    
    let position: Position?
}

struct Position: Codable {
    let latitude: Double?
    let longitude: Double?
}



class GalleryViewController: UIViewController, UISearchBarDelegate{
    
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var searchImagesBar: UISearchBar!
    
    
    @IBAction func show_favorites(_ sender: Any) {
        
        let favoritesViewPage: FavoritesViewController = self.storyboard?.instantiateViewController(withIdentifier: "FavoritesViewController") as! FavoritesViewController
        
                
        self.navigationController?.pushViewController(favoritesViewPage, animated: true)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.backBarButtonItem = UIBarButtonItem(
            title: "Gallery", style: .plain, target: nil, action: nil)
        
        imagesCollectionView.register(ImageCollectionViewCell.nib(), forCellWithReuseIdentifier: ImageCollectionViewCell.identifier)
        
        let layout = UICollectionViewFlowLayout()
        imagesCollectionView.collectionViewLayout = layout
        
        searchImagesBar.showsScopeBar = true
        searchImagesBar.delegate = self
        
    }
    
    var results: [Result] = []
    var location: Location?
    var location_str = ""
    
    var downloads: Int?
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()

        if let text = searchBar.text {
            results = []
            imagesCollectionView?.reloadData()
            fetchPhotos(query: text)
        }
    }
    

    func fetchPhotos(query: String){
        let urlString = "https://api.unsplash.com/search/photos?page=1&per_page=30&query=\(query)&client_id=urqf8qGu9zdu5sHBazk11YAar64BdOPIePZ__aZrknA"
        
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) {[weak self] data, _, error in
            guard let data = data, error == nil else {
                return
            }
            
            do {
                let jsonResult = try JSONDecoder().decode(UnsplashResponse.self, from: data)
                
                DispatchQueue.main.async {
                    self?.results = jsonResult.results
                                        
                    self?.imagesCollectionView.reloadData()
                }
            }
            catch{
                print(error)
            }
        }
        task.resume()
    }
    
    func getPhoto(img_id: String, finished: @escaping()->Void) {
  
        let urlString =
        "https://api.unsplash.com/photos/\(img_id)?client_id=urqf8qGu9zdu5sHBazk11YAar64BdOPIePZ__aZrknA"

        guard let url = URL(string: urlString) else {
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) {[weak self] data, _, error in
            guard let data = data, error == nil else {
                return
            }
            
            do {
                let jsonResult = try JSONDecoder().decode(Result.self, from: data)
                
                DispatchQueue.main.async {
                    
                    self?.downloads = jsonResult.downloads
                    self?.location = jsonResult.location
                    
                    finished()
                }
            }
            catch{
                print(error)
            }
            
        }
        task.resume()
    }
    
}

extension GalleryViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let imgViewPage: ImageViewController = self.storyboard?.instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
        
        imgViewPage.allow_like = true
        
        imgViewPage.selectedImage = results[indexPath.row].urls.regular
        
        self.getPhoto(img_id: results[indexPath.row].id){ [self] in
            
            imgViewPage.img_id = results[indexPath.row].id
            
            if results[indexPath.row].likes != nil {
                imgViewPage.total_likes_res = "Likes: " + String(results[indexPath.row].likes!)
            }
            else{
                imgViewPage.total_likes_res = "Likes: ?"
            }
                    
            var location = ""
            
                    
            if let country = self.location?.country {
                location += "Country:\(country)\n"
            }
            
            if let city = self.location?.city {
                location += "City:\(city)\n"
            }
            
         //   if let latitude = self.location?.position?.latitude{
           //     location += "Latitude:\(latitude)\n"
           // }
            
           // if let longtitude = self.location?.position?.longitude{
             //   location += "Longtitude:\(longtitude)\n"
           // }
            
            if location.isEmpty {
                imgViewPage.location_res = "Location: ?"
            }
            else {
                imgViewPage.location_res = location
            }

            if let d = self.downloads {
                imgViewPage.downloads_res = "Downloads: " + String(d)
                
            }
            else{
                imgViewPage.downloads_res = "Downloads: ?"
            }
            self.navigationController?.pushViewController(imgViewPage, animated: true)
        }
        
    }
}

extension GalleryViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return results.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let imageURLString = results[indexPath.row].urls.regular
        
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.identifier, for: indexPath) as? ImageCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.configure(with: imageURLString)
        
        return cell
    }
}

extension GalleryViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 120)
    }
}

