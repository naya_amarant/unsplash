//
//  ImageViewController.swift
//  Gallery
//
//  Created by Student on 3/25/22.
//

import UIKit
import FirebaseFirestore
import Firebase


class ImageViewController: UIViewController {

    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var heartImage: UIImageView!
    
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var downloadsLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    var selectedImage: String!
    
    var location_res: String!
    var total_likes_res: String!
    var downloads_res: String!
    var url_res: String!
    
    // get img_id for appending it to downloaded image title
    var img_id: String!
    
    var allow_like = true
    // ImageViewController is used to open image from Gallery and Favorites.
    // Favorites already includes liked image, so requires unable such ability in
    // FavoritesViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        image.isUserInteractionEnabled = true
        
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(didDoubleTap(_:)))
        
        tapGesture.numberOfTapsRequired = 2
        image.addGestureRecognizer(tapGesture)
    }
    
    
    func addToLocalFavorites(image_path: String){
        let path = "Users/student/Downloads/saved_imgs"
        
        let dir_url = URL(fileURLWithPath: path)
        
        // try create directory to store saved imgs
        do {
            try FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
            
            // name for saved img
            let imageName = dir_url.appendingPathComponent("Image_\(img_id!).png")

            if let imageUrl = URL(string: image_path){
            
                URLSession.shared.downloadTask(with: imageUrl) { (tempFileUrl, response, error) in
                    
                    if let imageTempFileUrl = tempFileUrl {
                        do {
                            let imageData = try Data(contentsOf: imageTempFileUrl)
                            try imageData.write(to: imageName)
                            
                        } catch {
                            print(error.localizedDescription)
                        }
                    }
                }.resume()
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    // remove from local by title
    func removeFromLocalFavorites(image_title: String) {
        let path = "Users/student/Downloads/saved_imgs"
        let dir_url = URL(fileURLWithPath: path)
        let image_url = dir_url.appendingPathComponent(image_title)
        
        let fileManager = FileManager.default
        do {
            try fileManager.removeItem(at: image_url)
            }
         catch {
            print(error)
        }
    }
    
    
    // add image to favorites
    @objc private func didDoubleTap(_ gesture: UITapGestureRecognizer){
        
        // allow_like - flag for viewing all gallery image or viewing favorite image
        if allow_like {
            // set heart only for image from gallery
            // and is connected
            if heartImage.isHidden && MainViewController.is_connected {
                heartImage.isHidden = false
                            
                let user_id = Auth.auth().currentUser!.uid
                let collection_name = Firestore.firestore().collection("img")
                let document_name = collection_name.document(String(user_id))
                
                // document name = user_id
                // if collection "img" with document name "user ID" doesn't exist,
                // create such document
                document_name.getDocument { (document, error) in
                   
                    self.addToLocalFavorites(image_path: self.selectedImage)
                    
                    if let document = document, document.exists {
                        
                        document_name.updateData([
                            "image_url": FieldValue.arrayUnion([self.selectedImage])
                        ])
                        
                    } else {
                        print("Document does not exist")
                        
                        collection_name.document(String(user_id)).setData([
                            "image_url": [String(self.selectedImage)],
                        ])
                    }
                }
            }
        }
        else{
            
            // if image was opened from FavoritesViewController,
            // delete it instead of like
            
            if MainViewController.is_connected {
                let user_id = Auth.auth().currentUser!.uid

                let document_ref = Firestore.firestore().collection("img").document(user_id)

                document_ref.updateData([
                    "image_url": FieldValue.arrayRemove([selectedImage!])
                ])
                
                // info about del img
                let alert = UIAlertController(title: "Image", message: "Image was deleted from favorites", preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }
            else {
                self.removeFromLocalFavorites(image_title: self.selectedImage)
                
                // info about del img
                let alert = UIAlertController(title: "Image", message: "Image was deleted from favorites", preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    static func loadImageFromDocumentDirectory(fileName: String) -> UIImage? {
        let path = "Users/student/Downloads/saved_imgs"
        
        let documentsUrl = URL(fileURLWithPath: path)
        let fileURL = documentsUrl.appendingPathComponent(fileName)
        do {
            let imageData = try Data(contentsOf: fileURL)
            return UIImage(data: imageData)
        } catch {}
        return nil
    }
    
    // show selected image
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if MainViewController.is_connected {
            guard let url = URL(string: selectedImage) else {
                return
            }
            do {
                let data = try Data(contentsOf: url)
                image.image = UIImage(data: data)
                
                // set such data if it was loaded from net
                // from gallery
                likesLabel.text = total_likes_res
                downloadsLabel.text = downloads_res
                locationLabel.text = location_res
                
            } catch let error as NSError{
                print(error)
            }
        }
        // load from local
        else {
            image.image = ImageViewController.loadImageFromDocumentDirectory(fileName: self.selectedImage)
        }
    }
}


