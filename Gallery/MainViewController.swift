//
//  MainViewController.swift
//  Gallery
//
//  Created by Student on 5/4/22.
//

import UIKit
import FirebaseAuth

class MainViewController: UIViewController {
    public static var is_connected = true
    
    func decide(){
        let isConnected = ConnectionManager.shared.hasConnectivity()
        print(isConnected)
        
        // check user defaults
        let user = Defaults()
        let user_data = user.getData()
        print("data: ", user_data)
        
        // if defaults exists and has connection
        // try login and show gallery
        if user_data.count > 0 && isConnected {
            
            FirebaseAuth.Auth.auth().signIn(withEmail: user_data[0], password: user_data[1], completion: { result, error in
                
                // if with signIn without error, show gallery
                if error == nil {
                    let galleryViewPage: GalleryViewController = self.storyboard?.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
                    self.navigationController?.pushViewController(galleryViewPage, animated: true)
                    print("gallery pushed")
                }
                // if with signIn we have error, defaults data is incorrect
                // need to login
                else {
                    let loginViewPage: ViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    print("view pushed")
                    self.navigationController?.pushViewController(loginViewPage, animated: true)
                }
            })
            
        }
        // if user defaults empty and has connection
        if user_data.count == 0 && isConnected {
            // go to login
            print ("view ")
            let loginViewPage: ViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            self.navigationController?.pushViewController(loginViewPage, animated: true)
        }
        // if hasn't connection, show favorites
        if isConnected == false {
            print("fav")
            let favoritesViewPage: FavoritesViewController = self.storyboard?.instantiateViewController(withIdentifier: "FavoritesViewController") as! FavoritesViewController
            
            // flag for favorites and image controllers
            MainViewController.is_connected = false
            
            self.navigationController?.pushViewController(favoritesViewPage, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.decide()
    }
}
