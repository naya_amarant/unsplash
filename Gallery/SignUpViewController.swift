//
//  LoginViewController.swift
//  Gallery
//
//  Created by Student on 3/24/22.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBAction func createAccountAction(_ sender: Any) {
        
        // empty email fied
        guard let email = emailTextField.text, !email.isEmpty
                else {
                    
                 let alert = UIAlertController(title: "Empty Email", message: "Please fill in email.", preferredStyle: UIAlertController.Style.alert)

                 alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

                 self.present(alert, animated: true, completion: nil)

                 return
             }
        
        // empty or incorrect
        guard let password1 = passwordTextField.text, !password1.isEmpty,
              let password2 = confirmPasswordTextField.text, !password2.isEmpty,
              password1 == password2
                else {
                    
                 let alert = UIAlertController(title: "Empty password fields or passwords doesn't match", message: "Please check password fields.", preferredStyle: UIAlertController.Style.alert)

                 alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

                 self.present(alert, animated: true, completion: nil)

                 return
             }
        
        // user creation
        FirebaseAuth.Auth.auth().createUser(withEmail: email, password: password1, completion: { result, error in
            guard error == nil else{
                
                let alert = UIAlertController(title: "Error", message: "Check your email address and try again", preferredStyle: UIAlertController.Style.alert)

                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

                self.present(alert, animated: true, completion: nil)
                
                return
            }
            
            let user = Defaults()

            // if user defaults data doesn't exist, we need to save it
            user.setData(login: email, password: password1)
            
            
            // show page with searchbar and images
            let galleryViewPage: GalleryViewController = self.storyboard?.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
            self.navigationController?.pushViewController(galleryViewPage, animated: true)
            
        })
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.emailTextField.text = ""
        self.passwordTextField.text = ""
        self.confirmPasswordTextField.text = ""
        
        do {
            try FirebaseAuth.Auth.auth().signOut()
        } catch let signOutError as NSError {
          print(signOutError)
        }
    }

}


