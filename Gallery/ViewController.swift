//
//  ViewController.swift
//  Gallery
//
//  Created by Student on 3/24/22.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {
    @IBOutlet weak var imageViewBG: UIImageView!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    // open SignUpController
    @IBAction func createAction(_ sender: Any) {
        let signUpViewPage: SignUpViewController = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(signUpViewPage, animated: true)
    }
    

    @IBAction func loginAction(_ sender: Any) {
        let user = Defaults()
        let user_data = user.getData()
        
        // if fields are empty
        guard let email = emailTextField.text, !email.isEmpty,
                let password = passwordTextField.text, !password.isEmpty
                else {
                    
                 let alert = UIAlertController(title: "Empty Email/Password", message: "Please fill in email/password.", preferredStyle: UIAlertController.Style.alert)

                 alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

                 self.present(alert, animated: true, completion: nil)

                 return
             }
                
        // after signIn show GalleryViewController
        FirebaseAuth.Auth.auth().signIn(withEmail: email, password: password, completion: { result, error in
            
            guard error == nil else {
                
                let alert = UIAlertController(title: "Incorrect data", message: "Please check email/password", preferredStyle: UIAlertController.Style.alert)

                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

                self.present(alert, animated: true, completion: nil)
                
                return
            }

            // if user defaults data exists and we can't login with Firebase,
            // probably it's wrong data. So we need to remove previous and set new
            if user_data.count > 0 {
                user.removeData(login: user_data[0], password: user_data[1])
                user.setData(login: email, password: password)
            }
            // if user defaults data doesn't exist, we need to save it
            else {
                user.setData(login: email, password: password)
            }
            
            let galleryViewPage: GalleryViewController = self.storyboard?.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
            self.navigationController?.pushViewController(galleryViewPage, animated: true)
            
        })
    }
    
    
    @IBAction func resetPasswordAction(_ sender: Any) {
        let resetViewPage: ForgotPasswordViewController = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(resetViewPage, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewBG.image = UIImage(named:"bg")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(
            title: "Sign Out", style: .plain, target: nil, action: nil)
        
        // need to logout
        self.passwordTextField.text = ""
        self.emailTextField.text = ""
       
        do {
            try FirebaseAuth.Auth.auth().signOut()
        } catch let signOutError as NSError {
          print(signOutError)
        }
    }
}

